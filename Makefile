PROGRAM = main
SHARED_MODULE = libshared.so

.PHONY: all clean

all: $(PROGRAM) $(SHARED_MODULE)

$(PROGRAM): main.c

$(SHARED_MODULE): shared.c
	gcc -shared -o $@ $^

clean:
	/bin/rm -rf main *.so *.o
