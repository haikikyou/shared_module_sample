#include <stdio.h>

void hello(const char *context) {
  printf("hello() in %s\n", context);
}
