#include <dlfcn.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>

char *p;

int main(int argc, char* argv[])
{
  void *func;
  void (*hello)(const char*);
  pid_t pid;
  p = (char*)malloc(128);
  strcpy(p, "hoge\n");

  func = dlopen("libshared.so", RTLD_LAZY);
  if (!func) {
    fprintf(stderr, "%s\n", dlerror());
    return 1;
  }

  *(void **)(&hello) = dlsym(func, "hello");
  if (!hello) {
    fprintf(stderr, "%s\n", dlerror());
    return 1;
  }

  pid = fork();
  if (pid < 0) {
    fprintf(stderr, "failed to fork: %d\n", errno);
    exit(EXIT_FAILURE);
  } else if (pid == 0) {
    *(void **)(&hello) = dlsym(func, "hello");
    if (!hello) {
      fprintf(stderr, "%s\n", dlerror());
      _exit(1);
    }
    strcpy(p, "foo\n");
    (*hello)("child");
    printf("[c] hello address = %p\n", hello);
    printf("[c] p address = %p\n", p);
    printf("p = %s\n", p);
    _exit(0);
  }

  waitpid(pid, NULL, 0);
    
  (*hello)("parent");
  printf("[p] hello address = %p\n", hello);
  printf("[p] p address = %p\n", p);
  printf("p = %s\n", p);
  free(p);

  return 0;
}
